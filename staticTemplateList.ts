import { FunctionComponent } from 'react';

import createLazyComponent from 'utils/createLazyComponent';
import { TemplateTheme, TodoRenameThisRenderCVProps } from '../types/template';

const Standard = createLazyComponent(() => import('./models/standard/Base'));
const TemplateTwo = createLazyComponent(() => import('./models/template_two/Base'));
const TemplateThree = createLazyComponent(() => import('./models/template_three/Base'));
const TemplateFour = createLazyComponent(() => import('./models/template_four/Base'));
const TemplateSix = createLazyComponent(() => import('./models/template_six/Base'));
const TemplateSeven = createLazyComponent(() => import('./models/template_seven/Base'));
const TemplateEight = createLazyComponent(() => import('./models/template_eight/Base'));
const TemplateNine = createLazyComponent(() => import('./models/template_nine/Base'));
const TemplateFive = createLazyComponent(() => import('./models/template_five/Base'));

type TemplateEntry = {
  key: TemplateTheme;
  name: string;
  component: FunctionComponent<TodoRenameThisRenderCVProps>;
};

export const allCVTemplates: TemplateEntry[] = [
  {
    key: TemplateTheme.STANDARD,
    name: 'Modèle 1',
    component: Standard,
  },
  {
    key: TemplateTheme.TEMPLATE_TWO,
    name: 'Modèle 2',
    component: TemplateTwo,
  },
  {
    key: TemplateTheme.TEMPLATE_THREE,
    name: 'Modèle 3',
    component: TemplateThree,
  },
  {
    key: TemplateTheme.TEMPLATE_FOUR,
    name: 'Modèle 4',
    component: TemplateFour,
  },
  {
    key: TemplateTheme.TEMPLATE_FIVE,
    name: 'Modèle 5',
    component: TemplateFive,
  },
  {
    key: TemplateTheme.TEMPLATE_SIX,
    name: 'Modèle 6',
    component: TemplateSix,
  },
  {
    key: TemplateTheme.TEMPLATE_SEVEN,
    name: 'Modèle 7',
    component: TemplateSeven,
  },
  {
    key: TemplateTheme.TEMPLATE_EIGHT,
    name: 'Modèle 8',
    component: TemplateEight,
  },
  {
    key: TemplateTheme.TEMPLATE_NINE,
    name: 'Modèle 9',
    component: TemplateNine,
  },
];

export default allCVTemplates;
