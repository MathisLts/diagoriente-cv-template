import React from 'react';
import classNames from 'common/utils/classNames';
import { ReactComponent as AvatarDefaultSvg } from 'assets/svg/modules/cv/avatar-default.svg';
import { TemplateEditor, TemplateEditorProps } from 'containers/ProfileContainer/SkillCardV3/types/template';

type Props = {
  data: any;
  editorData?: Partial<TemplateEditorProps>;
  config?: Partial<TemplateEditor>;
  customizationOptions: any;
};

const Header: React.FC<Props> = ({ data = {}, editorData = {}, config: configCV, customizationOptions }) => {
  const { photo, firstName, lastName, phone, email, extraInfos, job, keywords, description } = data;
  const { setCurrentStepByKey, currentKeyStep, showAssistant } = editorData;

  return (
    <div className="py-6 text-center items-center">
      <div className="grid justify-center text-center">
        {photo ? (
          <div onClick={() => setCurrentStepByKey?.('picture')} className="flex-shrink-0">
            <img
              src={photo}
              alt="Avatar CV"
              className={classNames(
                'rounded-full',
                showAssistant && currentKeyStep === 'picture'
                  ? 'border-2 border-lena2021-corail-dark'
                  : 'border-2 border-transparent  cursor-pointer',
              )}
              style={{ width: '80px', height: '80px', objectFit: 'cover', aspectRatio: '1 / 1' }}
            />
          </div>
        ) : (
          <div
            onClick={() => setCurrentStepByKey?.('picture')}
            className={classNames(
              'w-full cursor-pointer',
              showAssistant && currentKeyStep === 'picture'
                ? 'border-2 border-lena2021-corail-dark p-4'
                : 'border-2 border-transparent p-4 cursor-pointer',
            )}
            style={{
              background: `${customizationOptions?.accentColor}32`,
              alignItems: 'center',
              display: configCV?.preview ? 'none' : 'flex',
              justifyContent: 'center',
              width: '80px',
              height: '80px',
              objectFit: 'cover',
              aspectRatio: '1 / 1',
              borderRadius: '50%',
            }}
          >
            <AvatarDefaultSvg width="100%" />
          </div>
        )}
      </div>

      <div className="w-80 mx-auto mt-4">
        <div className="lw-full">
          <div>
            {/* First Name & Last Name */}
            <h2 className="text-3xl">
              {firstName} {lastName}
            </h2>

            {/* Job */}
            {job ? (
              <div
                onClick={() => setCurrentStepByKey?.('job')}
                className={classNames(
                  'border-2 border-transparent break-words',
                  showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                  showAssistant && currentKeyStep === 'job' && 'border-lena2021-corail-dark',
                )}
                style={{ fontSize: 15 }}
              >
                {job}
              </div>
            ) : (
              !configCV?.preview && (
                <div
                  onClick={() => setCurrentStepByKey?.('job')}
                  className={classNames(
                    'border-2 border-transparent break-words',
                    showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                    showAssistant && currentKeyStep === 'job' && 'border-lena2021-corail-dark',
                  )}
                >
                  <h3 className="uppercase font-bold">Métier, poste</h3>
                </div>
              )
            )}

            {((configCV?.preview && (email || phone)) || !configCV?.preview) && (
              <div
                onClick={() => setCurrentStepByKey?.('informations')}
                className={classNames(
                  'border-2 border-transparent',
                  showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                  showAssistant && currentKeyStep === 'informations' && 'border-lena2021-corail-dark',
                )}
              >
                <p className="break-words whitespace-pre-wrap">
                  {email || phone ? (
                    <div className="inline-flex">
                      <span>{email}</span> {' - '}
                      <span>{phone}</span>
                    </div>
                  ) : (
                    <h3 className="uppercase font-bold">E-mail, téléphone ...</h3>
                  )}
                </p>
              </div>
            )}
            {((configCV?.preview && extraInfos) || !configCV?.preview) && (
              <div
                onClick={() => setCurrentStepByKey?.('informations')}
                className={classNames(
                  'border-2 border-transparent',
                  showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                  showAssistant && currentKeyStep === 'informations' && 'border-lena2021-corail-dark',
                )}
              >
                {extraInfos?.length <= 0 && <h3 className="uppercase font-bold">Autres</h3>}

                <div>
                  <p className="break-words whitespace-pre-wrap">{extraInfos}</p>
                </div>
              </div>
            )}
          </div>

          <div>
            {((configCV?.preview && description) || !configCV?.preview) && (
              <div
                onClick={() => setCurrentStepByKey?.('description')}
                className={classNames(
                  'border-2 border-transparent',
                  showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                  showAssistant && currentKeyStep === 'description' && 'border-lena2021-corail-dark',
                )}
              >
                {description?.length <= 0 && <h3 className="uppercase font-bold">Description</h3>}
                <p className="break-words font-sans whitespace-pre-wrap">{description}</p>
              </div>
            )}
          </div>
        </div>
        {keywords && keywords.length > 0 && (
          <div
            onClick={() => setCurrentStepByKey?.('keywords')}
            className={classNames(
              'flex flex-wrap gap-1 mt-2',
              'border-2 border-transparent',
              showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
              showAssistant && currentKeyStep === 'keywords' && 'border-lena2021-corail-dark',
            )}
          >
            {keywords.map((k: any) => (
              <div
                key={k.id}
                style={{ borderRadius: 4, border: `2px solid ${customizationOptions?.accentColor}64` }}
                className="px-2 py-2 truncate"
              >
                {k.value}
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
