import React from 'react';
import { CVBase } from 'containers/ProfileContainer/SkillCardV3/types/dataCV';

type Props = {
  title?: string;
  data?: CVBase[];
};

const Skills: React.FC<Props> = ({ title, data }) => (
  <>
    <div className="font-semibold uppercase flex items-center">
      <span>{title}</span>
    </div>

    {data && (
      <ul className="list-inside list-disc">
        {data.map((d) => (
          <li key={d.id} className="break-words">
            {d.title}
          </li>
        ))}
      </ul>
    )}
  </>
);

export default Skills;
