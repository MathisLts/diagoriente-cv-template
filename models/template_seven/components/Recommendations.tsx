import React from 'react';
import classNames from 'common/utils/classNames';
import { transformDateTimePeriod } from '../../../helpers/parseCvDate';

type Props = {
  data: any;
  onClick?: () => void;
};

const Recommendations: React.FC<Props> = ({ data, onClick }) => {
  if (data.length <= 0) return null;

  return (
    <div onClick={() => onClick?.()} className={classNames('pbb-always relative space-y-4')}>
      <h2 className="font-semibold uppercase" style={{ fontSize: 16 }}>
        Recommandations
      </h2>

      {data.map((reco: any) => (
        <div key={reco.id} className="pbi-avoid">
          <strong className="flex font-semibold">
            De {`${reco.firstName} ${reco.lastName}`} {reco.structure && `— ${reco.structure}`}
          </strong>
          <span>
            Concernant : {reco.theme.title}{' '}
            <span className="uppercase">
              {reco.startDate && `— ${transformDateTimePeriod(reco.startDate, reco.endDate)}`}
            </span>
          </span>
          <p className="mt-2">{reco.response}</p>
        </div>
      ))}
    </div>
  );
};

export default Recommendations;
