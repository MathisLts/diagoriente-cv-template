import React, { useMemo } from 'react';
import { uniqueId } from 'lodash';
import { allCvBlocks, TodoRenameThisRenderCVProps } from 'containers/ProfileContainer/SkillCardV3/types/template';
import OtherData from 'containers/ProfileContainer/SkillCardV3/templates/models/template_eight/components/OtherData';
import { ExperienceCTA, ContentBlock } from '../../components';

import { Background, Header, Skill, Competence, Recommendations } from './components';
import Layout from './Layout';

const Base: React.FC<TodoRenameThisRenderCVProps> = ({ data, editorData, config, customizationOptions }) => {
  const { skills, competences, recommendations, overrides, contentOrder = [], volunteers } = data || {};
  const {
    showFirstUseExperienceCTA,
    showSecondUseCTA,
    setCurrentStepByKey,
    showAssistant = false,
    enableReordering = false,
    currentKeyStep,
  } = editorData || {};

  const attachOverride = (id: string) => {
    const findOverride = overrides?.competences.find((e: any) => e.id === id);
    return findOverride ? findOverride.title : undefined;
  };

  const renderCompetenceBlock = (contentKey: string, title: string, contentData: any[]) => {
    if (!contentData || !contentData.length) return null;

    return (
      <Competence
        title={title}
        data={contentData.map((e: any) => ({
          id: e.id,
          title: attachOverride(e.id) ?? e.title,
          visible: true,
        }))}
      />
    );
  };

  const renderSkillBlock = (contentKey: string, title: string, contentData: any[]) => {
    if (!contentData || !contentData.length) return null;

    const transformActivities = (acts: any[]) => {
      if (contentKey === 'volunteers') {
        return acts.flatMap((v) => ({
          id: uniqueId('local'),
          title: v.options.map((o: any) => o.title).join(' '),
        }));
      }

      return acts;
    };

    return (
      <Skill
        title={title}
        data={contentData.map((skill: any) => ({
          ...skill,
          title:
            skill.sourceType === 'USER_CREATED' ? skill.data.title : skill.data.intitule || skill.data.theme?.title,
          id: skill.data.id,
          activities: skill.sourceType === 'USER_CREATED' ? undefined : transformActivities(skill.data.activities),
          activity: skill.sourceType === 'USER_CREATED' ? skill.data.activities : undefined,
          extraActivity: skill.data.extraActivity || skill.data.extraOption || [],
          startDate: skill.data.startDate,
          endDate: skill.data.endDate,
          structure: skill.data.structure,
          visible: true,
        }))}
        customizationOptions={customizationOptions}
      />
    );
  };

  const renderContentBlock = (contentKey: string, currentIndex: number, maxIndex: number) => {
    const part = contentKey?.split('-')[0];
    const sub = contentKey?.split('-')[1];
    const title = allCvBlocks[contentKey];

    const firstElement = currentIndex === 0;
    const lastElement = currentIndex === maxIndex - 1;

    let componentBlock = null;

    switch (part) {
      case 'comp':
        componentBlock = renderCompetenceBlock(`comp-${sub}`, title, competences?.[sub]);
        break;
      case 'experience':
        componentBlock = renderSkillBlock(
          `experience-${sub}`,
          allCvBlocks[contentKey],
          skills?.filter((v: any) => v.type === sub) || [],
        );
        break;
      default:
        return null;
    }

    if (componentBlock) {
      return (
        <ContentBlock
          editorKey={contentKey}
          showBorderHover={showAssistant}
          showBorderActive={currentKeyStep === contentKey}
          showOrderUpButton={enableReordering && !firstElement}
          showOrderDownButton={enableReordering && !lastElement}
          onClick={() => setCurrentStepByKey?.(contentKey)}
          onMoveBlock={(direction: 'up' | 'down') => editorData?.moveContentBlock?.(contentKey, direction)}
        >
          {componentBlock}
        </ContentBlock>
      );
    }

    return null;
  };

  const columns = useMemo(() => {
    const array: any = [[], []];

    contentOrder.map((item, i) =>
      array[i % 2 ? 1 : 0]?.push(
        <div key={`cvblock_${item?.name}`}>{renderContentBlock(item?.name, i, contentOrder.length)}</div>,
      ),
    );

    return array;
  }, [contentOrder]);

  return (
    <Background>
      <Layout
        customizationOptions={customizationOptions}
        header={
          <>
            <Header config={config} data={data} editorData={editorData} customizationOptions={customizationOptions} />
            <OtherData
              config={config}
              data={data}
              editorData={editorData}
              customizationOptions={customizationOptions}
              rightContent={
                volunteers &&
                volunteers?.length > 0 && (
                  <ContentBlock
                    editorKey="volunteers"
                    showBorderHover={showAssistant}
                    showBorderActive={currentKeyStep === 'volunteers'}
                    showOrderUpButton={false}
                    showOrderDownButton={false}
                    onClick={() => setCurrentStepByKey?.('volunteers')}
                  >
                    {renderSkillBlock('volunteers', 'Service Civique', volunteers || [])}
                  </ContentBlock>
                )
              }
            />
          </>
        }
        content={
          <div className="p-4">
            {(showFirstUseExperienceCTA || showSecondUseCTA) && <ExperienceCTA />}

            <div className="space-y-5">
              <div className="grid grid-cols-2">
                <div>{columns[0]}</div>
                <div>{columns[1]}</div>
              </div>

              {(config?.preview || config?.showRecommendations) && <Recommendations data={recommendations} />}
            </div>
          </div>
        }
      />
    </Background>
  );
};

export default Base;
