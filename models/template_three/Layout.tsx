import React from 'react';
import { Background } from './components';

const Layout: React.FC<{
  sidebar?: JSX.Element;
  content: JSX.Element;
  customizationOptions: any;
}> = ({ sidebar, content }) => {
  return (
    <Background>
      <div className="flex space-x-10 h-full text-print">
        <div
          className="flex-shrink-0 h-full"
          style={{
            width: '35%',
          }}
        >
          {sidebar}
        </div>

        <div style={{ width: '60%' }}>{content}</div>
      </div>
    </Background>
  );
};

export default Layout;
