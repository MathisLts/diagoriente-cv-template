import React from 'react';
import classNames from 'common/utils/classNames';
import { ReactComponent as AvatarDefaultSvg } from 'assets/svg/modules/cv/avatar-default.svg';
import { TemplateEditor, TemplateEditorProps } from 'containers/ProfileContainer/SkillCardV3/types/template';

type Props = {
  data: any;
  editorData?: Partial<TemplateEditorProps>;
  config?: Partial<TemplateEditor>;
  customizationOptions: any;
};

const SideBar: React.FC<Props> = ({ data = {}, editorData = {}, config: configCV, customizationOptions }) => {
  const { photo, firstName, lastName, phone, email, extraInfos, job, formations, keywords, description } = data;
  const { setCurrentStepByKey, currentKeyStep, showAssistant } = editorData;

  return (
    <div className="p-6 h-full">
      {photo ? (
        <div onClick={() => setCurrentStepByKey?.('picture')}>
          <img
            src={photo}
            alt="Avatar CV"
            className={classNames(
              'rounded-full',
              showAssistant && currentKeyStep === 'picture'
                ? 'border-2 border-lena2021-corail-dark p-2'
                : 'border-2 border-transparent p-2 cursor-pointer',
            )}
            style={{ width: '100%', objectFit: 'cover', aspectRatio: '1 / 1' }}
          />
        </div>
      ) : (
        <div
          onClick={() => setCurrentStepByKey?.('picture')}
          className={classNames(
            'w-full cursor-pointer',
            showAssistant && currentKeyStep === 'picture'
              ? 'border-2 border-lena2021-corail-dark p-4'
              : 'border-2 border-transparent p-4 cursor-pointer',
          )}
          style={{
            background: `${customizationOptions?.accentColor}32`,
            alignItems: 'center',
            display: configCV?.preview ? 'none' : 'flex',
            justifyContent: 'center',
            width: '100%',
            aspectRatio: '1 / 1',
            borderRadius: '50%',
          }}
        >
          <AvatarDefaultSvg width="100%" />
        </div>
      )}

      {/* First Name & Last Name */}
      <h2 className="text-2xl mt-8">
        {firstName} {lastName}
      </h2>

      <div className="space-y-8">
        {/* Job */}
        {job ? (
          <div
            onClick={() => setCurrentStepByKey?.('job')}
            className={classNames(
              'border-2 border-transparent break-words',
              showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
              showAssistant && currentKeyStep === 'job' && 'border-lena2021-corail-dark',
            )}
          >
            {job}
          </div>
        ) : (
          !configCV?.preview && (
            <div
              onClick={() => setCurrentStepByKey?.('job')}
              className={classNames(
                'border-2 border-transparent break-words',
                showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                showAssistant && currentKeyStep === 'job' && 'border-lena2021-corail-dark',
              )}
            >
              <h3 className="uppercase font-bold">Métier, poste</h3>
            </div>
          )
        )}

        {/* Description, phone, emai */}
        <div
          className="border-b-2 pb-8"
          style={{
            borderColor: `${customizationOptions?.accentColor}32`,
          }}
        >
          {((configCV?.preview && description) || !configCV?.preview) && (
            <div
              onClick={() => setCurrentStepByKey?.('description')}
              className={classNames(
                'border-2 border-transparent',
                showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                showAssistant && currentKeyStep === 'description' && 'border-lena2021-corail-dark',
              )}
            >
              <h3 className="uppercase font-bold mb-1">Description</h3>
              <p className="break-words font-sans whitespace-pre-wrap">{description}</p>
            </div>
          )}

          {((configCV?.preview && (email || phone)) || !configCV?.preview) && (
            <div
              onClick={() => setCurrentStepByKey?.('informations')}
              className={classNames(
                'mt-5 border-2 border-transparent',
                showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                showAssistant && currentKeyStep === 'informations' && 'border-lena2021-corail-dark',
              )}
            >
              <p className="break-words whitespace-pre-wrap">
                {email || phone ? (
                  <div className="flex flex-col">
                    <h3 className="uppercase font-bold">CONTACT</h3>
                    <span>{email}</span>
                    <span>{phone}</span>
                  </div>
                ) : (
                  <h3 className="uppercase font-bold">E-mail, téléphone ...</h3>
                )}
              </p>
            </div>
          )}
        </div>

        {/* Formations */}
        {((configCV?.preview && formations && formations.length > 0) || !configCV?.preview) && (
          <div
            onClick={() => setCurrentStepByKey?.('formations')}
            className={classNames(
              'border-2 border-transparent',
              showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
              showAssistant && currentKeyStep === 'formations' && 'border-lena2021-corail-dark',
            )}
          >
            <h3 className="uppercase font-bold">Formations</h3>
            <ul className="break-words space-y-1">
              {formations &&
                formations.map((formation: any) => (
                  <li key={formation.id} className="break-words">
                    {formation.value}
                  </li>
                ))}
            </ul>
          </div>
        )}
        {/* Others */}
        {((configCV?.preview && extraInfos) || !configCV?.preview) && (
          <div
            onClick={() => setCurrentStepByKey?.('informations')}
            className={classNames(
              'border-2 border-transparent',
              showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
              showAssistant && currentKeyStep === 'informations' && 'border-lena2021-corail-dark',
            )}
          >
            <h3 className="uppercase font-bold">Autres</h3>

            <div>
              <p className="break-words whitespace-pre-wrap">{extraInfos}</p>
            </div>
          </div>
        )}

        {/* Keywords */}
        {keywords && keywords.length > 0 && (
          <div
            onClick={() => setCurrentStepByKey?.('keywords')}
            className={classNames(
              'flex flex-wrap gap-1',
              'border-2 border-transparent',
              showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
              showAssistant && currentKeyStep === 'keywords' && 'border-lena2021-corail-dark',
            )}
          >
            {keywords.map((k: any) => (
              <div
                key={k.id}
                style={{ borderRadius: 4, border: `2px solid ${customizationOptions?.accentColor}64` }}
                className="px-2 py-2 truncate"
              >
                {k.value}
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default SideBar;
