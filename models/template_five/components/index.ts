import Header from './Header';
import Background from './Background';
import Skill from './Skill';
import Competence from './Competence';
import Recommendations from './Recommendations';

export { Background, Header, Skill, Competence, Recommendations };
