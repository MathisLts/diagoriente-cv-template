import React, { FunctionComponent } from 'react';

const Background: FunctionComponent = ({ children }) => <div className="flex-1 flex flex-col h-full">{children}</div>;
export default Background;
