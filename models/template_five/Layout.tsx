import React from 'react';
import { Background } from './components';

const Layout: React.FC<{
  header?: JSX.Element;
  content: JSX.Element;
  customizationOptions: any;
}> = ({ header, content, customizationOptions }) => {
  return (
    <Background>
      <div className="flex flex-col h-full text-print">
        <div
          style={{
            ...(customizationOptions?.variant === 'fond' && { background: `${customizationOptions?.accentColor}16` }),
          }}
        >
          {header}
        </div>

        <div className="mt-10 h-full">{content}</div>
      </div>
    </Background>
  );
};

export default Layout;
