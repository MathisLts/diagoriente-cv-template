import React, { useMemo } from 'react';
import { uniqueId } from 'lodash';
import { allCvBlocks, TodoRenameThisRenderCVProps } from 'containers/ProfileContainer/SkillCardV3/types/template';
import Layout from './Layout';
import { Background, Header, Skill, Competence, Recommendations } from './components';
import { ExperienceCTA, ContentBlock } from '../../components';

const Base: React.FC<TodoRenameThisRenderCVProps> = ({ data, editorData, config: configCV, customizationOptions }) => {
  const { skills, competences, recommendations, overrides, contentOrder = [], volunteers } = data || {};
  const {
    showFirstUseExperienceCTA,
    showSecondUseCTA,
    setCurrentStepByKey,
    showAssistant = false,
    enableReordering = false,
    currentKeyStep,
  } = editorData || {};

  const attachOverride = (id: string) => {
    const findOverride = overrides?.competences.find((e: any) => e.id === id);
    return findOverride ? findOverride.title : undefined;
  };

  const renderCompetenceBlock = (contentKey: string, title: string, contentData: any[]) => {
    if (!contentData || !contentData.length) return null;

    return (
      <Competence
        title={title}
        data={contentData.map((e: any) => ({
          id: e.id,
          title: attachOverride(e.id) ?? e.title,
          visible: true,
        }))}
      />
    );
  };

  const renderSkillBlock = (contentKey: string, title: string, contentData: any[]) => {
    if (!contentData || !contentData.length) return null;

    const transformActivities = (acts: any[]) => {
      if (contentKey === 'volunteers') {
        return acts.flatMap((v) => ({
          id: uniqueId('local'),
          title: v.options.map((o: any) => o.title).join(' '),
        }));
      }
      return acts;
    };

    return (
      <Skill
        title={title}
        data={contentData.map((skill: any) => ({
          ...skill,
          title:
            skill.sourceType === 'USER_CREATED' ? skill.data.title : skill.data.intitule || skill.data.theme?.title,
          id: skill.data.id,
          activities: skill.sourceType === 'USER_CREATED' ? undefined : transformActivities(skill.data.activities),
          activity: skill.sourceType === 'USER_CREATED' ? skill.data.activities : undefined,
          extraActivity: skill.data.extraActivity || skill.data.extraOption || [],
          startDate: skill.data.startDate,
          endDate: skill.data.endDate,
          structure: skill.data.structure,
          visible: true,
        }))}
        customizationOptions={customizationOptions}
      />
    );
  };

  /**
   * @TODO : refactor this - add support for two columns
   */
  const renderContentBlock = (contentKey: string, currentIndex: number, maxIndex: number) => {
    const part = contentKey?.split('-')[0];
    const sub = contentKey?.split('-')[1];
    const title = allCvBlocks[contentKey];

    // If we are on two columns and we are at an odd number, the last
    // entry of the 2nd column cannot be moved downwards
    const maxIndexOverride = maxIndex % 2 !== 0 ? maxIndex - 2 : maxIndex - 1;

    const firstElement = currentIndex === 0;
    const lastElement = currentIndex === maxIndexOverride;

    let componentBlock = null;

    switch (part) {
      case 'comp':
        componentBlock = renderCompetenceBlock(`comp-${sub}`, title, competences?.[sub]);
        break;
      case 'experience':
      case 'volunteers':
        componentBlock = renderSkillBlock(
          part === 'volunteers' ? 'volunteers' : `experience-${sub}`,
          allCvBlocks[contentKey],
          part === 'volunteers' ? volunteers : skills?.filter((v: any) => v.type === sub) || [],
        );
        break;
      default:
        return null;
    }

    if (componentBlock) {
      return (
        <ContentBlock
          editorKey={contentKey}
          showBorderHover={showAssistant}
          showBorderActive={currentKeyStep === contentKey}
          showOrderUpButton={enableReordering && !firstElement}
          showOrderDownButton={enableReordering && !lastElement}
          onClick={() => setCurrentStepByKey?.(contentKey)}
          onMoveBlock={(direction: 'up' | 'down') => editorData?.moveContentBlock?.(contentKey, direction, 2)}
        >
          {componentBlock}
        </ContentBlock>
      );
    }

    return null;
  };

  const columns = useMemo(() => {
    const array: any = [[], []];

    contentOrder.map((item, i) =>
      array[i % 2 ? 1 : 0]?.push(
        <div key={`cvblock_${item?.name}`}>{renderContentBlock(item?.name, i, contentOrder.length)}</div>,
      ),
    );

    return array;
  }, [contentOrder]);

  return (
    <Background>
      <Layout
        customizationOptions={customizationOptions}
        header={
          <Header config={configCV} customizationOptions={customizationOptions} data={data} editorData={editorData} />
        }
        content={
          <div className="h-full">
            {(showFirstUseExperienceCTA || showSecondUseCTA) && <ExperienceCTA />}

            <div className="h-full">
              <div className="grid grid-cols-2 gap-2 divide-x h-full">
                <div className="p-4">{columns[0]}</div>
                <div className="p-4">{columns[1]}</div>
              </div>

              {(configCV?.preview || configCV?.showRecommendations) && <Recommendations data={recommendations} />}
            </div>
          </div>
        }
      />
    </Background>
  );
};

export default Base;
