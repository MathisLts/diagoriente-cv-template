import React from 'react';
import classNames from 'common/utils/classNames';
import { uniqueId } from 'lodash';
import { allCvBlocks, TodoRenameThisRenderCVProps } from 'containers/ProfileContainer/SkillCardV3/types/template';
import Layout from './Layout';
import { Background, SideBar, Skill, Competence, Recommendations } from './components';
import { ExperienceCTA, ContentBlock } from '../../components';

const Base: React.FC<TodoRenameThisRenderCVProps> = ({ data, editorData, config: configCV, customizationOptions }) => {
  const {
    firstName,
    lastName,
    job,
    skills,
    competences,
    recommendations,
    overrides,
    contentOrder = [],
    volunteers,
  } = data || {};
  const {
    showFirstUseExperienceCTA,
    showSecondUseCTA,
    setCurrentStepByKey,
    showAssistant = false,
    enableReordering = false,
    currentKeyStep,
  } = editorData || {};

  const attachOverride = (id: string) => {
    const findOverride = overrides?.competences.find((e: any) => e.id === id);
    return findOverride ? findOverride.title : undefined;
  };

  const renderCompetenceBlock = (contentKey: string, title: string, contentData: any[]) => {
    if (!contentData || !contentData.length) return null;

    return (
      <Competence
        title={title}
        data={contentData.map((e: any) => ({
          id: e.id,
          title: attachOverride(e.id) ?? e.title,
          visible: true,
        }))}
      />
    );
  };

  const renderSkillBlock = (contentKey: string, title: string, contentData: any[]) => {
    if (!contentData || !contentData.length) return null;

    const transformActivities = (acts: any[]) => {
      if (contentKey === 'volunteers') {
        return acts.flatMap((v) => ({
          id: uniqueId('local'),
          title: v.options.map((o: any) => o.title).join(' '),
        }));
      }
      return acts;
    };

    return (
      <Skill
        title={title}
        data={contentData.map(
          (skill: any) =>
            skill.data !== null && {
              ...skill,
              title:
                skill.sourceType === 'USER_CREATED' ? skill.data.title : skill.data.intitule || skill.data.theme?.title,
              id: skill.data.id,
              activities: skill.sourceType === 'USER_CREATED' ? undefined : transformActivities(skill.data.activities),
              activity: skill.sourceType === 'USER_CREATED' ? skill.data.activities : undefined,
              extraActivity: skill.data.extraActivity || skill.data.extraOption || [],
              startDate: skill.data.startDate,
              endDate: skill.data.endDate,
              structure: skill.data.structure,
              visible: true,
            },
        )}
        customizationOptions={customizationOptions}
      />
    );
  };

  const renderContentBlock = (contentKey: string, currentIndex: number, maxIndex: number) => {
    const part = contentKey?.split('-')[0];
    const sub = contentKey?.split('-')[1];
    const title = allCvBlocks[contentKey];

    const firstElement = currentIndex === 0;
    const lastElement = currentIndex === maxIndex - 1;

    let componentBlock = null;

    switch (part) {
      case 'comp':
        componentBlock = renderCompetenceBlock(`comp-${sub}`, title, competences?.[sub]);
        break;
      case 'experience':
      case 'volunteers':
        componentBlock = renderSkillBlock(
          part === 'volunteers' ? 'volunteers' : `experience-${sub}`,
          allCvBlocks[contentKey],
          part === 'volunteers' ? volunteers : skills?.filter((v: any) => v.type === sub) || [],
        );
        break;
      default:
        return null;
    }

    if (componentBlock) {
      return (
        <ContentBlock
          editorKey={contentKey}
          showBorderHover={showAssistant}
          showBorderActive={currentKeyStep === contentKey}
          showOrderUpButton={enableReordering && !firstElement}
          showOrderDownButton={enableReordering && !lastElement}
          onClick={() => setCurrentStepByKey?.(contentKey)}
          onMoveBlock={(direction: 'up' | 'down') => editorData?.moveContentBlock?.(contentKey, direction)}
        >
          {componentBlock}
        </ContentBlock>
      );
    }

    return null;
  };

  return (
    <Background>
      <Layout
        customizationOptions={customizationOptions}
        sidebar={
          <SideBar config={configCV} customizationOptions={customizationOptions} data={data} editorData={editorData} />
        }
        content={
          <>
            <div
              className="pb-5 mb-5"
              style={{
                borderBottom: `solid 1px ${customizationOptions?.accentColor}`,
              }}
            >
              {/* First Name & Last Name */}
              <h2 className="text-2xl">
                {firstName} {lastName}
              </h2>

              {((configCV?.preview && job) || !configCV?.preview) && (
                <div
                  onClick={() => setCurrentStepByKey?.('job')}
                  className={classNames(
                    'border-2 border-transparent break-words',
                    showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                    showAssistant && currentKeyStep === 'job' && 'border-lena2021-corail-dark',
                  )}
                >
                  {job || 'Métier, poste'}
                </div>
              )}
            </div>

            {(showFirstUseExperienceCTA || showSecondUseCTA) && <ExperienceCTA />}

            <div className="space-y-5">
              {contentOrder.map((item, i) => {
                return (
                  <div key={`cvblock_${item?.name}`}>{renderContentBlock(item?.name, i, contentOrder.length)}</div>
                );
              })}

              {(configCV?.preview || configCV?.showRecommendations) && <Recommendations data={recommendations} />}
            </div>
          </>
        }
      />
    </Background>
  );
};

export default Base;
