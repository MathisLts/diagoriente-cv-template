import React from 'react';
import { Background } from './components';

const Layout: React.FC<{ sidebar?: JSX.Element; content: JSX.Element; customizationOptions?: any }> = ({
  sidebar,
  content,
  customizationOptions,
}) => {
  return (
    <Background>
      <div className="flex space-x-10 h-full  text-print">
        <div
          className={`p-7 ${customizationOptions?.variant === 'trait'}`}
          style={{
            width: '70%',
            ...(customizationOptions?.variant === 'fond' && { background: `${customizationOptions?.accentColor}16` }),
            ...(customizationOptions?.variant === 'trait' && {
              borderRight: `1px solid ${customizationOptions?.accentColor}`,
            }),
          }}
        >
          {content}
        </div>

        <div
          style={{
            width: '30%',
            ...(customizationOptions?.variant === 'fond' && { background: `${customizationOptions?.accentColor}16` }),
          }}
          className="flex-shrink-0"
        >
          {sidebar}
        </div>
      </div>
    </Background>
  );
};

export default Layout;
