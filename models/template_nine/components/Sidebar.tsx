import React from 'react';
import classNames from 'common/utils/classNames';
import { ReactComponent as AvatarDefaultSvg } from 'assets/svg/modules/cv/avatar-default.svg';
import { TemplateEditor, TemplateEditorProps } from 'containers/ProfileContainer/SkillCardV3/types/template';

type Props = {
  data: any;
  editorData?: Partial<TemplateEditorProps>;
  config?: TemplateEditor;
  customizationOptions: any;
};

const SideBar: React.FC<Props> = ({ data = {}, editorData = {}, config, customizationOptions }) => {
  const { photo, email, phone, extraInfos, formations, keywords, description } = data;
  const { setCurrentStepByKey, currentKeyStep, showAssistant } = editorData;

  return (
    <div>
      {photo ? (
        <div className="mt-6" onClick={() => setCurrentStepByKey?.('picture')}>
          <img
            src={photo}
            alt=""
            className={classNames(
              showAssistant && currentKeyStep === 'picture'
                ? 'border-2 border-lena2021-corail-dark'
                : 'border-2 border-transparent cursor-pointer',
            )}
            style={{ width: '100%', height: '170px', objectFit: 'cover' }}
          />
        </div>
      ) : (
        <>
          {!config?.preview && (
            <div className="mt-6" onClick={() => setCurrentStepByKey?.('picture')}>
              <div
                className={classNames(
                  showAssistant && currentKeyStep === 'picture'
                    ? 'border-2 border-lena2021-corail-dark'
                    : 'border-2 border-transparent cursor-pointer',
                )}
                style={{
                  background: `${customizationOptions?.accentColor}20`,
                  width: '100%',
                  height: '170px',
                  alignItems: 'center',
                  display: config?.preview ? 'none' : 'flex',
                  justifyContent: 'center',
                }}
              >
                <AvatarDefaultSvg width={230} height={100} />
              </div>
            </div>
          )}
        </>
      )}

      {/* Description, email, phone */}
      <div
        className="p-7 text-center"
        style={{
          ...(customizationOptions?.variant === 'trait' && {
            marginTop: '32px',
            borderTop: `1px solid ${customizationOptions?.accentColor}`,
          }),
        }}
      >
        {((config?.preview && description) || !config?.preview) && (
          <div
            onClick={() => setCurrentStepByKey?.('description')}
            className={classNames(
              'border-2 border-transparent',
              showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
              showAssistant && currentKeyStep === 'description' && 'border-lena2021-corail-dark',
            )}
          >
            <p className="break-words whitespace-pre-wrap">
              {description || <h3 className="uppercase font-bold">Description</h3>}
            </p>
          </div>
        )}

        <div className="space-y-5">
          {((config?.preview && (email || phone)) || !config?.preview) && (
            <div
              onClick={() => setCurrentStepByKey?.('informations')}
              className={classNames(
                'mt-5 border-2 border-transparent',
                showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                showAssistant && currentKeyStep === 'informations' && 'border-lena2021-corail-dark',
              )}
            >
              <p className="break-words whitespace-pre-wrap">
                {email || phone ? (
                  <div className="flex flex-col">
                    <h3 className="uppercase font-bold">CONTACT</h3>
                    <span>{email}</span>
                    <span>{phone}</span>
                  </div>
                ) : (
                  <h3 className="uppercase font-bold">E-mail, téléphone ...</h3>
                )}
              </p>
            </div>
          )}

          {/* Formations */}
          {((config?.preview && formations && formations.length > 0) || !config?.preview) && (
            <div
              onClick={() => setCurrentStepByKey?.('formations')}
              className={classNames(
                'border-2 border-transparent',
                showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                showAssistant && currentKeyStep === 'formations' && 'border-lena2021-corail-dark',
              )}
            >
              <h3 className="uppercase font-bold">Formations</h3>
              <ul className="break-words space-y-1">
                {formations &&
                  formations.map((formation: any) => (
                    <li key={formation.id} className="break-words">
                      {formation.value}
                    </li>
                  ))}
              </ul>
            </div>
          )}

          {/* Others */}
          {((config?.preview && extraInfos) || !config?.preview) && (
            <div
              onClick={() => setCurrentStepByKey?.('informations')}
              className={classNames(
                'border-2 border-transparent',
                showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                showAssistant && currentKeyStep === 'informations' && 'border-lena2021-corail-dark',
              )}
            >
              <div>
                <h3 className="uppercase font-bold">Autres</h3>
              </div>
              <div>
                <p className="break-words whitespace-pre-wrap">{extraInfos}</p>
              </div>
            </div>
          )}

          {/* Keywords */}
          {keywords && keywords.length > 0 && (
            <div
              onClick={() => setCurrentStepByKey?.('keywords')}
              className={classNames(
                'flex flex-wrap gap-1',
                'border-2 border-transparent',
                showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                showAssistant && currentKeyStep === 'keywords' && 'border-lena2021-corail-dark',
              )}
            >
              {keywords.map((k: any) => (
                <div
                  key={k.id}
                  className="border-2 border-lena-gray-light p-2 truncate"
                  style={{
                    borderRadius: 4,
                    border: `2px solid ${customizationOptions?.accentColor}64`,
                  }}
                >
                  {k.value}
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default SideBar;
