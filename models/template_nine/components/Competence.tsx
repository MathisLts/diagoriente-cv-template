import React from 'react';
import { CVBase } from 'containers/ProfileContainer/SkillCardV3/types/dataCV';

type Props = {
  title?: string;
  data?: CVBase[];
  customizationOptions?: any;
};

const Competence: React.FC<Props> = ({ title, data, customizationOptions }) => (
  <div>
    <div className="flex items-center">
      <h2 className="font-semibold uppercase">{title}</h2>
    </div>

    <ul className="list-inside list-disc">
      {data &&
        data.map((d) => (
          <li key={d.id} className="break-words">
            {d.title}
          </li>
        ))}
    </ul>
  </div>
);

export default Competence;
