import Background from './Background';
import SideBar from './Sidebar';
import Skill from './Skill';
import Competence from './Competence';
import Recommendations from './Recommendations';

export { Background, SideBar, Skill, Competence, Recommendations };
