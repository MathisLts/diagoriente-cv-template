import React from 'react';
import { Background } from './components';

const Layout: React.FC<{
  header?: JSX.Element;
  content: JSX.Element;
  customizationOptions: any;
}> = ({ header, content }) => {
  return (
    <Background>
      <div className="flex flex-col h-full text-print">
        <div>{header}</div>

        <div>{content}</div>
      </div>
    </Background>
  );
};

export default Layout;
