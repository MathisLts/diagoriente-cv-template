import React from 'react';
import { TemplateEditor, TemplateEditorProps } from 'containers/ProfileContainer/SkillCardV3/types/template';
import classNames from 'common/utils/classNames';

type Props = {
  data: any;
  editorData?: Partial<TemplateEditorProps>;
  config?: Partial<TemplateEditor>;
  customizationOptions: any;
  rightContent?: any;
};

const OtherData: React.FC<Props> = ({
  data = {},
  editorData = {},
  config: configCV,
  customizationOptions,
  rightContent,
}) => {
  const { formations } = data;
  const { setCurrentStepByKey, currentKeyStep, showAssistant } = editorData;

  if (!rightContent && formations.length === 0 && (configCV?.preview || configCV?.print)) return <div />;

  return (
    <div
      style={{
        borderTop: `10px solid ${customizationOptions?.accentColor}16`,
        borderBottom: `10px solid ${customizationOptions?.accentColor}16`,
      }}
    >
      <div className="grid grid-cols-2 gap-8 py-4 px-10">
        <div>
          {((configCV?.preview && formations && formations.length > 0) || !configCV?.preview) && (
            <div
              onClick={() => setCurrentStepByKey?.('formations')}
              className={classNames(
                'border-2 border-transparent',
                showAssistant && 'hover:border-lena2021-corail-dark cursor-pointer',
                showAssistant && currentKeyStep === 'formations' && 'border-lena2021-corail-dark',
              )}
            >
              <h3 className="uppercase font-bold">Formations</h3>
              <ul className="break-words gap-y-1">
                {formations &&
                  formations.map((formation: any) => (
                    <li key={formation.id} className="break-words">
                      {formation.value}
                    </li>
                  ))}
              </ul>
            </div>
          )}
        </div>
        <div>{rightContent}</div>
      </div>
    </div>
  );
};

export default OtherData;
