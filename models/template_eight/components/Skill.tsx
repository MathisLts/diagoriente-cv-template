import React from 'react';
import { CVBase } from 'containers/ProfileContainer/SkillCardV3/types/dataCV';
import { transformDateTimePeriod } from '../../../helpers/parseCvDate';

type Props = {
  title?: string;
  data?: CVBase[];
  customizationOptions: any;
};

const Skill: React.FC<Props> = ({ title, data, customizationOptions }) => {
  const recommendations = data
    ?.filter(
      (_) =>
        _?.data?.recommendations?.find((reco: any) => reco?.status === 'accepted') &&
        _.data?.recommendations.length > 0,
    )
    .map((_) => _?.data?.recommendations);
  const hasRecommendations = (recommendations?.length || 0) > 0;

  return (
    <>
      <div className="flex items-center">
        <h2 className="font-semibold uppercase mb-3">{title}</h2>
      </div>

      {data && (
        <div>
          {data.map((d) => (
            <div key={d.id} className="pbi-avoid">
              <strong>{d.title}</strong>

              <div className="font-normal">
                {d.structure && `${d.structure}`} {d.structure && d.startDate && ' | '}
                {transformDateTimePeriod(d.startDate, d.endDate)}
              </div>

              {d.activity && <p className="whitespace-pre-line ml-6">{d.activity}</p>}

              {d.activities && (
                <ul className="list-inside list-disc pt-2">
                  {d.activities.map((activity) => (
                    <li key={activity.id} className="ml-4">
                      {activity.title}
                    </li>
                  ))}
                </ul>
              )}

              {d.extraActivity && (
                <ul className="list-inside list-disc">
                  {d.extraActivity.map((activity) => (
                    <li key={activity} className="ml-4">
                      {activity}
                    </li>
                  ))}
                </ul>
              )}
            </div>
          ))}
        </div>
      )}

      {hasRecommendations && (
        <p
          className="inline-flex mt-3 text-lena2021-blue-dark px-2 py-1 rounded-md"
          style={{
            border: `solid 1px ${customizationOptions?.accentColor}`,
          }}
        >
          RECOMMANDATION
        </p>
      )}
    </>
  );
};

export default Skill;
