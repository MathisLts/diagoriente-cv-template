import Background from './Background';
import Header from './Header';
import Skill from './Skill';
import Competence from './Competence';
import Recommendations from './Recommendations';

export { Background, Header, Skill, Competence, Recommendations };
