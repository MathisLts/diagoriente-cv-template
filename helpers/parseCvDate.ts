import moment from 'moment';

/**
 * parseDate
 * @param {string} date
 * @return {string} Janvier 2022
 */
export const parseDate = (date?: string) => moment(date).format('MMMM YYYY');

/**
 * transformDateTimePeriod
 * @param {string} dateStart
 * @param {string} dateEnd
 * @return {string} Janvier 2022 - Mars 2022
 */
export const transformDateTimePeriod = (dateStart?: string, dateEnd?: string) => {
  if (!dateStart && !dateEnd) return '';

  return `${parseDate(dateStart)} - ${dateEnd ? parseDate(dateEnd) : 'Présent'}`;
};
