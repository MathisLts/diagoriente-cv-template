import React, { FunctionComponent, useState } from 'react';
import useCvCreate from 'hooks/useCvCreate';

import { Button } from 'components/design-system';
import ModalComponent from 'components/design-system/Modal';

import ModalShowExperiences from 'containers/ProfileContainer/SkillCardV3/components/modals/ModalShowExperiences';
import DropdownAddExperience from 'containers/ProfileContainer/SkillCardV3/components/assistant/DropdownAddExp';

import { ReactComponent as WizardIcon } from 'assets/svg/modules/cv/wizard.svg';

const ExperienceCTA: FunctionComponent = () => {
  const { cvId } = useCvCreate();

  const [showDropdownAddExp, setShowDropdownAddExp] = useState(false);
  const [showModalExperience, setShowModalExperiences] = useState(false);

  return (
    <div className=" p-8 bg-lena2021-corail-light">
      <div className="mx-8 flex flex-col items-center justify-center text-center">
        <div className="text-3xl text-lena-corail font-bold mt-7">Ajoutez vos expériences</div>

        <p className="text-xl mt-4 mb-6">Nous ajouterons automatiquement les compétences qui vont avec</p>

        <WizardIcon className="h-16 w-16" />

        <div className={'relative mt-6'}>
          <Button
            variant={'orange'}
            extraClasses={'text-lg'}
            onClick={() => setShowDropdownAddExp(!showDropdownAddExp)}
          >
            Ajouter une expérience
          </Button>
          {showDropdownAddExp && (
            <DropdownAddExperience
              cvId={cvId}
              onExperience={() => setShowModalExperiences(true)}
              onClose={() => setShowDropdownAddExp(false)}
              textLg
            />
          )}
        </div>
      </div>
      {showModalExperience && (
        <ModalComponent
          open={true}
          variant="interactive"
          allowOutsideClick={false}
          enableGlassEffect={false}
          onClose={() => setShowModalExperiences(false)}
        >
          <ModalShowExperiences cvId={cvId} onClose={() => setShowModalExperiences(false)} />
        </ModalComponent>
      )}
    </div>
  );
};

export default ExperienceCTA;
