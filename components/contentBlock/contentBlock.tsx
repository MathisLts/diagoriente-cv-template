import React from 'react';
import classNames from 'common/utils/classNames';
import { ReactComponent as ArrowDownSvg } from 'assets/svg/generic/arrow_down.svg';

type Props = {
  editorKey: string;
  showBorderHover: boolean;
  showBorderActive: boolean;
  onClick?: () => void;
  showOrderUpButton?: boolean;
  showOrderDownButton?: boolean;
  onMoveBlock?: (dir: 'up' | 'down') => void;
  children: React.ReactNode;
};

const ContentBlock: React.FC<Props> = ({
  showBorderHover,
  showBorderActive,
  onClick,
  showOrderUpButton,
  showOrderDownButton,
  onMoveBlock,
  children,
}) => {
  const handleMove = (e: React.MouseEvent<HTMLButtonElement>, dir: 'up' | 'down') => {
    e.stopPropagation();

    onMoveBlock?.(dir);
  };

  return (
    <div
      onClick={() => onClick?.()}
      className={classNames(
        'relative border-2 border-transparent p-2',
        showBorderHover && 'hover:border-lena2021-corail-dark cursor-pointer',
        showBorderActive && 'border-lena2021-corail-dark',
      )}
    >
      {(showOrderUpButton || showOrderDownButton) && (
        <div className="z-75 space-x-2 absolute right-0 inline-flex">
          {showOrderUpButton && (
            <button
              onClick={(e) => handleMove(e, 'up')}
              className="relative flex justify-center items-center bg-white rounded-lg border-2 border-lena-blue-dark w-8 h-6 transform rotate-180 hover:shadow-lg"
            >
              <ArrowDownSvg width={13} height={13} className="self-center" />
            </button>
          )}

          {showOrderDownButton && (
            <button
              onClick={(e) => handleMove(e, 'down')}
              className="relative flex justify-center items-center bg-white rounded-lg border-2 border-lena-blue-dark w-8 h-6 hover:shadow-lg"
            >
              <ArrowDownSvg width={13} height={13} className="self-center" />
            </button>
          )}
        </div>
      )}

      {children}
    </div>
  );
};

export default ContentBlock;
