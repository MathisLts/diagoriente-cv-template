import React, { useRef, useMemo } from 'react';
import { Template } from '../types/template';
import Base from './models/standard/Base';
import staticTemplateList from './staticTemplateList';

const InitializeTemplate = React.forwardRef<HTMLDivElement, Template>(
  ({ theme, data, editorData, config, customizationOptions }, ref) => {
    const templateRef = useRef<HTMLDivElement>(null);

    const recommendations = useMemo(() => {
      return [
        ...(data?.skills || []).flatMap((exp: any) => {
          return (
            exp?.data?.recommendations
              ?.filter((_: any) => _.status === 'accepted')
              ?.filter(Boolean)
              ?.map((reco: any) => ({
                ...reco,
                type: 'skills',
                startDate: exp.data.startDate,
                endDate: exp.data.endDate,
                theme: exp.data.theme,
              })) || []
          );
        }),
        ...(data?.volunteers?.flatMap((exp: any) => {
          return (
            exp?.data?.recommendations
              ?.filter((_: any) => _.status === 'accepted')
              ?.filter(Boolean)
              ?.map((reco: any) => ({
                ...reco,
                type: 'volunteers',
                startDate: exp.data.startDate,
                endDate: exp.data.endDate,
                theme: exp.data.theme,
              })) || []
          );
        }) || []),
      ];
    }, [data?.skills, data?.volunteers]);

    const heightTemplate = useMemo(() => {
      const needle = (ref as any)?.current?.clientHeight;
      if (needle) {
        return [1122, 2244, 3367, 4492, 5615].find((v) => v >= needle) || 0 + (recommendations.length > 0 ? 1122 : 0);
      }

      return 1122;
    }, [ref, data]);

    const renderTemplate = () => {
      const staticHost =
        process.env.NODE_ENV === 'production'
          ? 'https://static.diagoriente.beta.gouv.fr/'
          : 'https://static.diagoriente.fr/';
      const avatarUrl = data?.photo || '';
      const fixupUrl = avatarUrl.replace(/^(undefined)/, staticHost);

      const Component = staticTemplateList.find((v) => v.key === theme)?.component || Base;

      const skillsCustom = useMemo(() => {
        const skills = [...data?.skills];
        const skillsFilter: any = [];

        skills.map((s) => {
          const d =
            s.data?.activities?.filter?.((e: any) => s.activities?.find((m: any) => e.id === m.activityId)) ||
            undefined;

          skillsFilter.push({
            ...s,
            data: {
              ...s.data,
              activities: typeof s.activities === 'undefined' ? s.data.activities : d,
            },
          });
        });

        return skillsFilter;
      }, [data]);

      return (
        <div style={{ height: config?.preview || !config?.print ? '100%' : heightTemplate }} ref={templateRef}>
          <div className="h-full" ref={ref}>
            <Component
              data={{
                ...data,
                skills: skillsCustom,
                recommendations,
                contentOrder: data?.contentOrder?.filter((_) => _.visible),
                photo: fixupUrl,
              }}
              editorData={editorData}
              customizationOptions={customizationOptions || { variant: 'trait', accentColor: '' }}
              config={config}
            />
          </div>
        </div>
      );
    };

    return renderTemplate();
  },
);

export default InitializeTemplate;
